<?php
/**
 * @file
 * Class functions for the eAssistance Pro Live Chat
 */

/**
 * Declatre Class.
 */
class EAssistancePro {
  protected static $instance = NULL;
  protected $moduledir = NULL;

  /**
   * Function get Instance.
   */
  public static function getInstance() {
    if (is_null(self::$instance)) {
      self::$instance = new eassistancepro();
    }
    return self::$instance;
  }

  /**
   * Constructor.
   */
  protected function __construct() {
    $this->moduledir = drupal_get_path('module', 'eassistancepro');
  }

  /**
   * Reset Settings.
   */
  public function resetSettings() {
    variable_del('eassistancepro_account_info');
    variable_del('eassistancepro_username');
    variable_del('eassistancepro_password');
    variable_del('eassistancepro_mid');
  }

  /**
   * Validation.
   */
  public function validate($str) {
    if (trim($str) == "") {
      return FALSE;
    }
  }

  /**
   * Cheeck if module installed.
   */
  public function isInstalled() {
    $eassistancepro_account_info = variable_get('eassistancepro_account_info');
    $eassistancepro_username = variable_get('eassistancepro_username');
    $eassistancepro_password = variable_get('eassistancepro_password');
    $eassistancepro_mid = variable_get('eassistancepro_mid');
    if (is_null($eassistancepro_account_info)) {
      return FALSE;
    }
    if (is_null($eassistancepro_username)) {
      return FALSE;
    }
    if (is_null($eassistancepro_password)) {
      return FALSE;
    }
    if (is_null($eassistancepro_mid)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Check is chat button installed.
   */
  public function chatButtonInstalled() {
    if ($this->isInstalled() == FALSE) {
      return FALSE;
    }
    $result = db_query('SELECT status FROM {block} WHERE module=:module', array(':module' => 'eassistancepro'));
    $row = $result->fetchObject();
    if (!$row || $row->status != '1') {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Get Chat button code.
   */
  public function getChatCode() {
    $eassistancepro = EAssistancePro::getInstance();
    if ($eassistancepro->chatButtonInstalled()) {
      $eassistancepro_account_info = variable_get('eassistancepro_account_info');
      $eassistancepro_username = variable_get('eassistancepro_username');
      $eassistancepro_password = variable_get('eassistancepro_password');
      $eassistancepro_mid = variable_get('eassistancepro_mid');
      $return = "<script language=\"javascript\" src=\"https://www.eassistancepro.com/script.php?account=" . $eassistancepro_username . "&mid=" . $eassistancepro_mid . "&html=1\"></script><a href=\"javascript:void(0);\" onclick=\"requestChat('" . $eassistancepro_username . "', '" . $eassistancepro_mid . "', '0');\"><img src=\"https://www.eassistancepro.com/status.php?dep=&mid=" . $eassistancepro_mid . "\" border=\"0\"></a>";
      return $return;
    }
  }

  /**
   * Connect to eassistancepro host and get live chat button code.
   */
  public function getRemote($url, $port = 80) {
    if (function_exists("curl_init")) {
      $curl_handle = curl_init();
      curl_setopt($curl_handle, CURLOPT_URL, $url);
      curl_setopt($curl_handle, CURLOPT_PORT, $port);
      curl_setopt($curl_handle, CURLOPT_HEADER, FALSE);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, 2);
      curl_setopt($curl_handle, CURLOPT_POST, FALSE);
      $data = curl_exec($curl_handle);
      curl_close($curl_handle);
    }
    else {
      $data = file_get_contents($url);
    }
    return $data;
  }
}
