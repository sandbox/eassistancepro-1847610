********************************************************************
                     D R U P A L    M O D U L E
********************************************************************

Name: eAssistance Pro
Author: Kalla
Tags: live chat, chat, support
Drupal: 7
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

********************************************************************
Description
********************************************************************

Get Live Chat services from eAssitance pro on your Drupal powered website
through this module.

This module allows you to easily incorporate chat services on your website
without any dexterous knowledge of HTML and codes.

eAssistance pro is a remarkable solution which integrates live chat, visitor
tracking and help desk services on your websites. Let your visitors contact
you anytime through live chat and even if you are unable they can send offline
messages and emails to let you always be in reach of your customers.

Quickly add eAssistance pro services on your website with few simple clicks
and be live within 5 minutes or less.

You need to have an existing eAssistance pro subscription plan or signup
with us for free to kick-start engaging sessions of customer-merchant
relationship.

Try it free with all functional benefits for 30 days!

Thanks,
http://www.eassistancepro.com

********************************************************************
Screenshots
********************************************************************
http://www.eassistancepro.com/screenshots.php


********************************************************************
Features
********************************************************************
http://www.eassistancepro.com/features.php


********************************************************************
Frequently Asked Questions
********************************************************************
http://www.eassistancepro.com/faqs.php


********************************************************************
Changelog
********************************************************************
= 1.0 =
First release  


********************************************************************
Upgrade Notice
********************************************************************
First release
