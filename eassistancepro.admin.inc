<?php
/**
 * @file
 * Helper functions for the eAssistance Pro Live Chat.
 */

/**
 * EAssistane Pro form structure.
 */
function eassistancepro_admin_settings_form($form_state) {
  global $language, $base_url;
  $eassistancepro = EAssistancePro::getInstance();
  if ($eassistancepro->isInstalled()) {
    if ($eassistancepro->chatButtonInstalled()) {
      $form['live_chat'] = array(
        '#type' => 'item',
        '#markup' => t('<div class="messages status">eAssistance Pro is installed properly.</div>'),
      );
    }
    else {
      $form['live_chat'] = array(
        '#type' => 'item',
        '#markup' => t('<div class="messages warning">Please activate the eAssistance Pro block in !url.</div>', array('!url' => l(t('Structure') . ' > ' . t('Blocks'), $base_url . '/admin/structure/block'))),
      );
    }
  }
  else {
    $form['live_chat'] = array(
      '#type' => 'item',
      '#markup' => t('<div class="messages warning">Please enter your eAssistance Pro username and password'),
    );
  }

  $form['general'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#prefix' => '<p>This module allows you to easily incorporate chat services on your website without any dexterous knowledge of HTML and codes. eAssistance Pro is a remarkable solution which integrates live chat, visitor tracking and help desk services on your websites. Let your visitors contact you anytime through live chat and even if you are unable they can send offline messages and emails to let you always be in reach of your customers. Quickly add eAssistance Pro services on your website with few simple clicks and be live within 5 minutes or less. You need to have an existing eAssistance Pro subscription plan or signup with us for free to kick-start engaging sessions of customer-merchant relationship.</p><div><h3>Connect</h3>',
    '#suffix' => '</div>',
  );

  $form['general']['eassistancepro_account_info'] = array(
    '#type' => 'textfield',
    '#title' => t('Email Address or Username'),
    '#size' => '30',
    '#maxlength' => 50,
    '#default_value' => variable_get('eassistancepro_account_info', ''),
    '#description' => t('Enter your eAssistance Pro Account Name'),
  );

  $form['general']['eassistancepro_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#size' => '30',
    '#maxlength' => 50,
    '#default_value' => variable_get('eassistancepro_password', ''),
    '#description' => t('Enter your eAssistance Pro Account Password'),
  );

  if ($eassistancepro->isInstalled()) {
    if ($eassistancepro->chatButtonInstalled()) {
      $form['general']['reset'] = array(
        '#type' => 'submit',
        '#value' => t('Reset settings'),
      );
    }
  }
  return system_settings_form($form);
}

/**
 * EAssistane Pro Form Validaion.
 */
function eassistancepro_admin_settings_form_validate($form, &$data) {
  $eassistancepro = EAssistancePro::getInstance();
  if (isset($data['input']) && isset($data['input']['op']) && $data['input']['op'] === 'Reset settings') {
    $eassistancepro->resetSettings();
  }
  else {
    $response = EAssistancePro::getRemote('https://www.eassistancepro.com/app_login.php?email=' . $data['input']['eassistancepro_account_info'] . '&password=' .      $data['input']['eassistancepro_password'] . '&type=drupal');
    $response = htmlspecialchars_decode($response, ENT_QUOTES);
    $response_array = explode("::", $response);
    if (!strstr($response, "Error") === FALSE) {
      form_set_error('Error', t($response));
    }
    else {
      variable_set('eassistancepro_account_info', $data['input']['eassistancepro_account_info']);
      variable_set('eassistancepro_username', $response_array[0]);
      variable_set('eassistancepro_password', $data['input']['eassistancepro_password']);
      variable_set('eassistancepro_mid', $response_array[1]);
    }
  }
}
